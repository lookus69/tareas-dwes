<?php
include_once 'Conexion.php';
/**
 * Gestiona la info de los jugadores
 * y se encarga de guardar y recuperar los datos
 * asi como otras consultas a la base de datos.
 * 
 *  @author lookus69
 */
class Jugador extends Conexion{

  private $nombre, $apellidos, $dorsal, $posicion, $codigo;

  /**
   * Comprueba si existe el codigo en la DB.
   * @param int|string $codigo el numero del codigo de barras a comprobar.
   * @return bool si el codigo existe o no.
   */
  public static function existeCodigo($codigo){
    $sql = "SELECT barcode FROM jugadores WHERE barcode = $codigo";
    return count(self::leer($sql)) > 0;
  }//existeCodigo()

  /**
   *  Comprueba si hay jugadores en la DB.
   *  @return boolean si hay jugadores o no.
   */
  public static function hayJugadores(){
    $sql = "SELECT id FROM jugadores";
    return count(self::leer($sql)) > 0;
  }//hayJugadores()

  /**
   * Comprueba si el dorsal ya se ha asignado 
   * a algun otro jugador en la DB.
   * @param int $dorsal el numero de dorsal a comprobar
   * @return bool si el numero de doral ya está asignado o no.
   */
  public static function comprobarDorsal($dorsal){
    $sql = "SELECT dorsal FROM jugadores WHERE dorsal = $dorsal";
    return count(self::leer($sql)) > 0;
  }//comprobarDorsal()

  /**
   * Carga las opciones posibles desde la DB para las
   * posiciones de los jugadores y devuelve un array con dichas posiciones.
   * 
   * @return string[] un array con las opciones posibles para las posiciones de los jugadores 
   */
  public static function getPosiciones(){
    #realiza una consulta para obtener los datos de la columna 'posicion' de la tabla 'jugadores'
    $posiciones = self::leer('SHOW COLUMNS FROM jugadores LIKE \'posicion\'');
    if($posiciones){
      #reemplaza el texto 'enum' el parentesis de apertura y el de cierre (y su contenido) por el contenido entre los parentesis
      #y luego separa esa cadena en un array usando ',' como separador.
      $posiciones = explode("','", preg_replace("/(enum)\('(.+?)'\)/", "$2", $posiciones[0]['Type']));
    }else{
      $posiciones = ['no se encontraron datos'];
    }
    return $posiciones;
  }//getPosiciones()

  public static function getListado(){
    $sql = 'SELECT * FROM jugadores';
    return self::leer($sql);
  }

  public static function guardarJugador($nombre, $apellidos, $dorsal, $posicion, $codigo){
    if($dorsal){
      $sql = "INSERT INTO jugadores (nombre, apellidos, dorsal, posicion, barcode) VALUES ('$nombre', '$apellidos', $dorsal, '$posicion', '$codigo')";
    }else{
      $sql = "INSERT INTO jugadores (nombre, apellidos, posicion, barcode) VALUES ('$nombre', '$apellidos', '$posicion', '$codigo')";
    }
    return self::escribir($sql);
  }//guardarJugador()

}//class Jugador