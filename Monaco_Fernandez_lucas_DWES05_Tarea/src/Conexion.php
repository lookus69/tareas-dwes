<?php
class Conexion{
  
  private static function conecta(){
    $host = 'localhost';
    $db = 'practicaUnidad5';
    $usr = 'daw';
    $pass = 'daw';
    $dsn = "mysql:host=$host;dbname=$db";
    try{
      $conexion = new PDO($dsn, $usr, $pass);
      return $conexion;
    }catch(PDOException $e){
      return null;
    }
  }//conecta()
  private static function desconecta(&$conexion){  
    //la conexion se pasa por referencia para poderla cerrar, sino lo que se pondría a null seria la referencia al objeto y no el propio objeto
    //o algo asi entendi yo! :D
    $conexion = null;
  }//desconecta()

  protected static function escribir($sql){
    $conexion = self::conecta();
    if(!$conexion){
      return null;
    }
    try{
      $resultados = $conexion->exec($sql);
      self::desconecta($conexion);
      return $resultados;
    }catch(PDOException $e){
      self::desconecta($conexion);
      return null;
    }

  }//create()

  protected static function leer($sql){
    $conexion = self::conecta();
    if(!$conexion){
      return null;
    }
    try{
      $consulta = $conexion->query($sql);
      $resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);  #(PDO::FETCH_ASSOC);
      return $resultados;
    }catch (PDOException $e){
      return null;
    }
  }//read()

//lookus
}//class Conexion


