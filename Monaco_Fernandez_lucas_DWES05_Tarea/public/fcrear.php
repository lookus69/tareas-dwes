<?php
@include '../src/debug.php';  #se suprimen los mensajes de error en esta sentencia pues se supone que este archivo no existirá en produccion.
session_start();
require '../vendor/autoload.php';
require_once '../src/Jugador.php';
#use RyanChandler\Blade\Blade;
use Jenssegers\Blade\Blade;
#$datosFormulario = [];

$datosFormulario = $_SESSION['datosFormulario'] ?? [];
$errores = $_SESSION['errores'] ?? [];

/*
Al utilizar un enlace a otra pagina para generar el codigo
se borran los datos del formulario, pues solo llegan al servidor mediante post
pero como enunciado de la tarea manda hacerlo con un enlace, creo que la 
solucion mas sencilla es redirigir a la pagina que genera el codigo al 
cargar la pagina si dicho codigo no se ha generado aun.
ya que el codigo no se puede editar, creo que seria mejor generarlo en esta
pagina directamente, pero no es lo que pide la tarea...
*/
if(!isset($datosFormulario['codigo'])){
  header('Location: generarCode.php');
}
$views = '../views';
$cache = '../cache';
$blade = new Blade($views,$cache);
$titulo = 'Crear Jugador';
$encabezado = $titulo;
$contenido = '<b>el contenido va aqui</b>';
$opcionesPosicion = Jugador::getPosiciones();
#$datosFormulario = ['nombre'=> '', 'apellidos'=>''];
echo $blade->make('vcrear', compact('titulo','encabezado','contenido', 'opcionesPosicion', 'datosFormulario', 'errores'))->render();
?>