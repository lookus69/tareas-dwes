<?php
@include '../src/debug.php';
require_once '../src/Jugador.php';
session_start();
$datosFormulario = [];
$errores = [];
if($_SERVER['REQUEST_METHOD'] =='POST'){
  foreach($_POST as $k => $v){
    $datosFormulario[$k] = htmlspecialchars(stripslashes(trim($v)));
    #$t = gettype($v); $l = strlen($v); $n = is_numeric($v);
    #echo "$k: $v \t tipo: $t \t len: $l \t isnum: $n<br>";
  }
  if(strlen($datosFormulario['dorsal']) == 0){
    $datosFormulario['dorsal'] = 0;
  }
  if(!is_numeric($datosFormulario['dorsal'])){
    $errores[] = 'El dorsal debe ser un numero o estar vacio';
  }
   if(Jugador::comprobarDorsal($datosFormulario['dorsal'])){
    $errores[] = 'El dorsal seleccionado pertenece a otro jugador';
  }
  if(strlen($datosFormulario['nombre']) == 0 | strlen($datosFormulario['apellidos']) == 0){
    $errores[] = 'El nombre y los apellidos son obligatorios';
  }
  if(strlen($datosFormulario['codigo']) != 12){
    $errores[] = 'Debe generar un codigo de barras';
  }
}//if POST
if(count($errores) == 0){
  session_unset();
  $ok = Jugador::guardarJugador($datosFormulario['nombre'], $datosFormulario['apellidos'], $datosFormulario['dorsal'], $datosFormulario['posicion'], $datosFormulario['codigo']);
  if($ok){
    $_SESSION['estado'] = 'creado';
    $_SESSION['mensaje'] = 'Jugador Creado con exito';
  }else{
    $_SESSION['estado'] = 'error';
    $_SESSION['mensaje'] = 'Error al guardar el jugador en la base de datos';
  }
  header('Location: jugadores.php');
  exit;

//ToDo: todo ok, guardar, limpiar sesion y volver
echo "guardado: $ok<br>";
echo 'sesion info: ' . $_SESSION['ok'] . "\t" . $_SESSION['mensaje'];
}else{
  
  $imagenCodigo = $_SESSION['datosFormulario']['imagenCodigo'] ?? null;
  $_SESSION['datosFormulario'] = $datosFormulario;
  $_SESSION['datosFormulario']['imagenCodigo'] = $imagenCodigo;
  $_SESSION['errores'] = $errores;
  header('Location: fcrear.php');
}

