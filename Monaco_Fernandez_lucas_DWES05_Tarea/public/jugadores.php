<?php
@include '../src/debug.php';
session_start();
require '../vendor/autoload.php';
require_once '../src/Jugador.php';
use Jenssegers\Blade\Blade;
use Milon\Barcode\DNS1D;
$titulo = $encabezado = 'Listado de Jugadores';
$mensaje = ['estado' => $_SESSION['estado'] ?? '', 'mensaje' => $_SESSION['mensaje'] ?? ''];
$jugadores = Jugador::getListado();
$barcode = new DNS1D;
$barcode -> setStorPath('../cache/');
#var_dump(array_keys($jugadores[0]));
$blade = new Blade('../views', '../cache');
echo $blade -> make('vjugadores',compact('titulo', 'encabezado', 'mensaje','jugadores','barcode')) -> render();

?>