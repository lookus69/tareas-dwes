<?php
session_start();
session_unset();
session_destroy();
require_once '../src/Jugador.php';
$jugadores = new Jugador();
if($jugadores -> hayJugadores()){
  header('Location: ./jugadores.php');
  exit;
}
header('Location: ./fcrear.php');
exit;
?>