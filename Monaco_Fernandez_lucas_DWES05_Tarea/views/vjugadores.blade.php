@extends('plantillas.html')
@section('mensaje')
  @isset($mensaje['estado'])
  <div class="{{$mensaje['estado']}}">
    <p>{{$mensaje['mensaje']}}</p>
  </div>
  @endisset
@endsection
@section('nuevoJugador')
  <a href="fcrear.php" class="boton ok">+ Nuevo Jugador</a>
@endsection
@section('tabla')
  <table id="tablaJugadores">
    <thead>
      <tr>
        @foreach($jugadores[0] as $campo => $valor)
          <th>{{ucfirst($campo)}}</th>
        @endforeach
      </tr>
    </thead>
    <tbody>
    @foreach($jugadores as $jugador)
    <tr>
      @foreach($jugador as $campo => $valor)
      <td>
        @if($campo == 'dorsal' && $valor == 0)
          Sin Asignar
        @elseif($campo == 'barcode')
          <img src="data:image/png;base64,{{$barcode->getBarcodePNG($valor, 'EAN13', 2, 35, array(255,255,255), false)}}" alt="codigo de barras">
          
        @else
          {{$valor}}
        @endif
      </td>
      @endforeach
    </tr>
    @endforeach
    </tbody>
  </table>
@endsection


@section('contenido')
  @yield('mensaje')
  @yield('nuevoJugador')
  @yield('tabla')
@endsection