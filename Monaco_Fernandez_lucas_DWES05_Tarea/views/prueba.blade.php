<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="estilo.css">
    <title>{{$titulo}}</title>
  </head>
  <body>
    <h2>{{$encabezado}}</h2>
    {{$contenido}}
  </body>
</html>