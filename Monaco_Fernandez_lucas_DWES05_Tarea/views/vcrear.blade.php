@extends('plantillas.html')
@section('select')
  @foreach ($opcionesPosicion as $posicion)
    @if($datosFormulario['posicion'] == $posicion)
    <option selected>{{$posicion}}</option>
    @else
    <option>{{$posicion}}</option>
    @endif
  @endforeach
@endsection
@section('errores')
  <div class="error">
    @foreach($errores as $error)
    <p>{{$error}}</p>
    @endforeach
  </div>
@endsection

@section('formulario')
  <form action="crearJugador.php" method="post">
    <div class="campoformulario">
      <label for="nombre">Nombre</label><br>
      <input type="text" name="nombre" id="nombre" value="{{$datosFormulario['nombre']??''}}">
    </div>
    <div class="campoformulario">
      <label for="apellidos">Apellidos</label>
      <input type="text" name="apellidos" id="apellidos" value="{{$datosFormulario['apellidos']??''}}">
    </div>
    <div class="campoformulario">
      <label for="dorsal">Dorsal</label>
      <input type="number" name="dorsal" id="dorsal" value="{{$datosFormulario['dorsal']??''}}">
    </div>
    <div class="campoformulario">
      <label for="posicion">Posición</label>
      <select name="posicion" id="posicion">
        @yield('select')
      </select>
    </div>    
    <div class="campoformulario">
      <label for="codigo">Código de Barras</label>
      <input type="text" name="codigo" id="codigo" readonly="readonly" value="{{$datosFormulario['codigo']??''}}">
      @isset($datosFormulario['imagenCodigo'])
      <img src="{{$datosFormulario['imagenCodigo']}}" alt="Codigo de Barras {{$datosFormulario['codigo']??''}}">
      @endisset
    </div>
    <div class="campoformulario botones">
      <input type="submit" value="Crear" class="boton neutro">
      <input type="reset" value="Limpiar" class="boton ok">
      <a href="index.php" class="boton volver">
        Volver
      </a>
      <a href="generarCode.php" class="boton dark"><img>Generar Barcode</a>
    </div> 
    
  </form>

@endsection
@section('contenido')
  @yield('errores')
  @yield('formulario')
@endsection