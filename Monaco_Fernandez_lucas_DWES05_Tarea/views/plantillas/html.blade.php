<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="estilo.css">
    <title>{{$titulo}}</title>
  </head>
  <body>
    <div class="contenedor">
    <h2>{{$encabezado}}</h2>
    @yield('contenido')
    </div>
    <footer style="position:fixed;right:5%;bottom:5%;opacity:0.3;">lucas Mónaco Fernández</footer>
  </body>
</html>