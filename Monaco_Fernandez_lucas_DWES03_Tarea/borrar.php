<?php
if($_SERVER["REQUEST_METHOD"] != "GET"){
    inicio();
}
$id = trim($_GET["id"]);
if(!$id){
    inicio();
}
require_once "db.php";
$respuesta = "";
if(!existeProducto($id)){
    $respuesta = "<span class=\"error\">El producto con codigo $id NO existe!</span>";
}else{
    if(borrarProducto($id)){
        $respuesta = "<span>El producto con codigo $id se ha borrado correctamente.</span>";
    }else{
        $respuesta = "<span class=\"error\">Ha habido algun error al intentar borrar el producto con codigo $id.</span>";
}
}

function inicio(){
    header("Location:listado.php");
}
?>
<!doctype html>
<html lang=es">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="estilo.css"/>
        <title>Borrar Producto - lucas Mónaco Fernández</title>
    </head>
    <body>
        <?php echo $respuesta;?>
        <a href="listado.php" class="boton volver">Volver</a>
    </body>
</html>