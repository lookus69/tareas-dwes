<?php
require_once "db.php";
include_once "firma.php";
//var_dump(listarProductos());
function mostrarTabla($listado){
    echo "<table>";
    echo "<tr><th>Detalle</th><th>Codigo</th><th>Nombre</th><th>Acciones</th></tr>";
    foreach($listado as $fila){
        mostrarFila($fila);
    }

    echo "</table>";
}
function mostrarFila($fila){
    echo "<tr><td><a class=\"boton detalle\" href=\"./detalle.php?id={$fila["id"]}\">Detalle</a></td>";
    echo "<td>{$fila["id"]}</td>";
    echo "<td>{$fila["nombre"]}</td>";
    echo "<td><a class=\"boton actualizar\" href=\"./update.php?id={$fila["id"]}\">Actualizar</a>";
    //echo "<a class=\"boton borrar\" href=\"./borrar.php?id={$fila["id"]}\">Borrar</a></td></tr>";
    echo "<form method=\"get\" action=\"borrar.php\">";
    echo "<input type=\"submit\" class=\"boton borrar\" value=\"Borrar\">";
    echo "<input type=\"hidden\" name=\"id\" value=\"{$fila["id"]}\"></form>";
}
?>
<!Doctype html>
<html lang="es">
    <meta charset="utf-8">
    <head>
        <title>Listado de Productos - lucas Mónaco Fernández</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>Gestión de Productos</h1>
        <div>
            <a class="boton crear" href="./crear.php">Crear</a>
            <?php
            mostrarTabla(listarProductos());
            echo firma();
            ?>
        </div>
    </body>
</html>