<?php
if(!$_SERVER["REQUEST_METHOD"]== "GET"){
    inicio();
}
$id = trim($_GET["id"]);
if(!$id){
    inicio();
}
require_once "db.php";
if(!existeProducto($id)){
    inicio();
}
$detalles = detallesProducto($id);

function inicio(){
    header("Location:listado.php");
}//inicio()
?>
<!doctype html>
<html lang="es">
    <meta charset="utf-8"/>
    <head>
        <title>Detalle del producto <?php echo $detalles["nCorto"];?> - lucas Mónaco Fernández</title>
        <link rel="stylesheet" href="estilo.css"/>
    </head>
    <body>
        <h1>Detalle Producto</h1>
        <div class="contenedor">
            <div class="titulo">
                <?php echo $detalles["nombre"];?>
            </div>
            <h4>Codigo:<?php echo $detalles["id"];?></h4>
            <p>Nombre: <?php echo $detalles["nombre"];?></p>
            <p>Nombre Corto: <?php echo $detalles["nCorto"];?></p>
            <p>Codigo Familia <?php echo $detalles["familia"];?></p>
            <p>PVP (€): <?php echo $detalles["pvp"];?></p>
            <p>Descripcion: <?php echo $detalles["descripcion"];?></p>
        </div>
        <div class="centrado">
        <a class="boton volver" href="detalle.php">Volver</a>
        </div>
        <?php include_once "firma.php"; echo firma();?>
    </body>
</html>