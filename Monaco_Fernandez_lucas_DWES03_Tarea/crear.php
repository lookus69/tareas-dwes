<?php
require_once "db.php";
$errorNombre = $errorNCorto = $errorPrecio = "";
$nombre = $nCorto = $precio = $descripcion = "";
$creado = 0;
if($_SERVER["REQUEST_METHOD"]=="POST"){
    $nombre = trim($_POST["nombre"]);
    $nCorto = trim($_POST["nCorto"]);
    $precio = trim($_POST["precio"]);
    $familia = trim($_POST["familia"]);
    $descripcion = trim($_POST["descripcion"]);
    
    if(!$nombre){
        $errorNombre = "*Este campo no puede estar vacio";
    }
    if(strlen($nombre)>200){
        $errorNombre = "*Este campo no puede contener mas de 200 caracteres";
    }
    if(!$nCorto){
        $errorNCorto = "*Este campo no puede estar vacio";
    }
    if(strlen($nCorto)> 50){
        $errorNCorto = "*Este campo no puede contener mas de 50 caracteres";
    }
    if(!$precio){
        $errorPrecio = "*Este campo no puede estar vacio";
    }
    if(strlen($precio)>12){
        $errorPrecio = "*Este campo no puede tener mas de 12 caracteres";
    }
    if(!is_numeric($precio)){
        $errorPrecio = "*Este campo debe contener un valor numerico";
    }
    if(!$errorNombre && !$errorNCorto && !$errorPrecio){
        $creado = crearProducto($nombre, $nCorto, $precio, $familia, $descripcion);
        
    }//if todo ok
}//if post
?>
<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>Crear Producto - lucas Mónaco Fernández</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>Crear Producto</h1>
<?php
if($creado>0){?>
        <div>
        <span>El producto se ha creado correctamente</span>
        <a href="listado.php" class="boton volver">Volver</a>
        </div>
<?php
}
if($creado<0){?>
    <div>
        <span class="error">Se ha producido un error al intentar insertar el producto en la base de datos</span>
        <a href="listado.php" class="boton volver">Volver</a>
    </div>
     
<?php
}
if($creado==0){?>        
        <div>
            <form action="crear.php" method="POST" class="anchoTotal">
                <div class="columna">
                    <label for="nombre">Nombre</label>
                    <span class="campoError"><?php echo $errorNombre;?></span><br/>
                    <input type="text" name="nombre" id="nombre" placeholder="Nombre">
                </div>
                <div class="columna">
                    <label for="nCorto">Nombre Corto</label>
                    <span class="campoError"><?php echo $errorNCorto;?></span><br/>
                    <input type="text" name="nCorto" id="nCorto" placeholder="Nombre Corto">
                </div>
                <div class="columna">
                    <label for="precio">Precio (€)</label>
                    <span class="campoError"><?php echo $errorPrecio;?></span><br>
                    <input type="text" name="precio" id="precio" placeholder="Precio (€)">
                </div>
                <div class="columna">
                    <label for="familia">Familia</label><br/>
                    <select name="familia" id="familia">
                        <?php
                        $familias = getFamilias();
                        foreach($familias as $familia){
                            echo "<option value=\"{$familia["cod"]}\">{$familia["nombre"]}</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="columnaDoble">
                    <label for="descripcion">Descripcion</label><br/>
                    <textarea name="descripcion" id="descripcion"></textarea>
                </div>
                <div class="columna">
                    <input type="submit" class="boton crear" value="Crear"/>
                    <input type="reset" class="boton limpiar" value="Limpiar"/>
                    <a href="listado.php" class="boton volver">Volver</a>
                </div>
            </form>
        </div>
<?php } 
    include_once "firma.php"; echo firma();?>
    </body>
</html>