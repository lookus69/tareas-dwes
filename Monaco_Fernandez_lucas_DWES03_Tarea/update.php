<?php
if(($_SERVER["REQUEST_METHOD"]!="POST") && !($_GET["id"])){
    header("Location:listado.php");
}else{
    $id = trim($_GET["id"]);
    
}
require_once "db.php";
$errorNombre = $errorNCorto = $errorPrecio = "";

function mostrarFormulario($producto){
    global $errorNombre,$errorNCorto,$errorPrecio;
?>
    <div>
       <form action="update.php" method="POST" class="anchoTotal">
           <div class="columna">
               <label for="nombre">Nombre</label>
               <span class="campoError"><?php echo $errorNombre;?></span><br/>
               <input type="text" name="nombre" id="nombre" value="<?php echo $producto["nombre"];?>">
           </div>
           <div class="columna">
               <label for="nCorto">Nombre Corto</label>
               <span class="campoError"><?php echo $errorNCorto;?></span><br/>
               <input type="text" name="nCorto" id="nCorto" value="<?php echo $producto["nCorto"];?>">
           </div>
           <div class="columna">
               <label for="precio">Precio (€)</label>
               <span class="campoError"><?php echo $errorPrecio;?></span><br>
               <input type="text" name="precio" id="precio" value="<?php echo $producto["pvp"];?>">
           </div>
           <div class="columna">
               <label for="familia">Familia</label><br/>
               <select name="familia" id="familia">
                   <?php
                   $familias = getFamilias();
                   $familiaProducto = $producto["familia"];
                   foreach($familias as $familia){
                       $selected = ($familiaProducto == $familia["cod"])?"selected":"";
                       echo "<option value=\"{$familia["cod"]}\" $selected>{$familia["nombre"]}</option>";
                   }
                   ?>
               </select>
           </div>
           <div class="columnaDoble">
               <label for="descripcion">Descripcion</label><br/>
               <textarea name="descripcion" id="descripcion"><?php echo $producto["descripcion"];?></textarea>
           </div>
           <div class="columna">
               <input type="submit" class="boton modificar" value="Modificar"/>
               <a href="listado.php" class="boton volver">Volver</a>
           </div>
           <input type="hidden" value="<?php echo $producto["id"];?>" name="id"/>
       </form>
   </div>
<?php
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Actualizar Producto - lucas Mónaco Fernández</title>
        <link rel="stylesheet" href="estilo.css"/>
    </head>
    <body>
        <h1>Modificar Producto</h1>
<?php
if($id){
    $producto = is_numeric($id)?detallesProducto($id):false;
    if(!$producto){
?>
        <div class="centrado">
        <span class="error">El codigo del producto (<?php echo $id;?>) no existe en la base de datos!</span>
        <a href="listado.php" class="boton volver">Volver</a>
        </div>
<?php
    }else{
        mostrarFormulario($producto);
    }
}
?>
<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
    $id = trim($_POST["id"]);
    $nombre = trim($_POST["nombre"]);
    $nCorto = trim($_POST["nCorto"]);
    $precio = trim($_POST["precio"]);
    $familia = trim($_POST["familia"]);
    $descripcion = trim($_POST["descripcion"]);
    
    if(!$nombre){
        $errorNombre = "*Este campo no puede estar vacio";
    }
    if(strlen($nombre)>200){
        $errorNombre = "*Este campo no puede contener mas de 200 caracteres";
    }
    if(!$nCorto){
        $errorNCorto = "*Este campo no puede estar vacio";
    }
    if(strlen($nCorto)> 50){
        $errorNCorto = "*Este campo no puede contener mas de 50 caracteres";
    }
    if(!$precio){
        $errorPrecio = "*Este campo no puede estar vacio";
    }
    if(strlen($precio)>12){
        $errorPrecio = "*Este campo no puede tener mas de 12 caracteres";
    }
    if(!is_numeric($precio)){
        $errorPrecio = "*Este campo debe contener un valor numerico";
    }
    if(!$errorNombre && !$errorNCorto && !$errorPrecio){
        $actualizado= actualizarProducto($id, $nombre, $nCorto, $precio, $familia, $descripcion);
        $mensaje = $actualizado?"Los datos del producto se han actualizado correctamente":"Se ha producido un error al intentar actualizar la informacion en la base de datos";
        $clase = $actualizado?"ok":"error";
        ?>
        <div class="centrado">
            <span class="<?php echo $clase;?>"><?php echo $mensaje;?></span>
            <a href="listado.php" class="boton volver">Volver</a>
        </div>
        <?php
    }else{//if !todo ok
        mostrarFormulario(detallesProducto($id));
    }
}
?>
        <?php include_once "firma.php"; echo firma();?>
    </body>
</html>