<?php
/*Esta funcion establece la conexion con la base de datos y devuelve
el objeto PDO con la conexion o false si no se ha podido establecer la conexion
*/
function conectar($servidor = "localhost", $usuario = "daw", $clave = "daw", $db = "proyecto"){
    $dsn = "mysql:host=$servidor;dbname=$db;charset=utf8mb4";
    //return false; //esta linea sirve para probar la reaccion ante un eventual error de conexion con la DB
    try{
        $conexion = new PDO($dsn,$usuario,$clave);
        return $conexion;
    }catch(PDOException $e){
        return false;
    }
}//conectar()

/*Esta funcion se utiliza para hacer consultas a la BBDD que devuelven resultados
(SELECT)(sería la R de cRud). primero establece la conexion (usando la funcion 
conectar) y comprueba si se ha establecido.
se crea un array y si hay resultados se van introduciendo los datos de cada fila
por ultimo se cierra la conexion y se devuelve el array.
*/
function read($consulta){
    $sql = conectar();
    if(!$sql){
        return false;
    }
    $resultados = $sql->query($consulta);
    $datos = [];
    while($fila = $resultados->fetch(PDO::FETCH_ASSOC)){
        array_push($datos,$fila);
    }
    $sql = null;
    return $datos;
}//read

/*Esta funcion se utiliza para hacer consultas de accion, que no devuelven 
un conjunto de datos, sino el numero de campos afectados por la consulta
(INSERT, UPDATE y DELETE)(Serian las C, U y D de CrUD)
al igual que la anterior, establece la conexion, comprueba que exista y
ejecuta la consulta, devolviendo el numero de campos afectados como resultado
despues de cerrar la conexion.
*/
function accion($consulta){
    $sql = conectar();
    if(!$sql){
        return 0;
    }
    $afectados = $sql->exec($consulta);
    return $afectados;
}//accion()

function listarProductos(){
    $consulta = "SELECT id, nombre from productos";
    $listado = read($consulta);
    return $listado;
}//listarProductos()

/*comprueba si existe el producto en la BBDD
    como la clave primaria es numerica, si la cadena recibida no 
    es una cadena numerica, el producto no existe.
    a continuacion se ejecuta una consulta buscando el id solicitado
    y se hace un cast a boolean al resultado obtenido, pues si no hay
    ningun registro, 0 evalua a false, en cualquier otro caso evalua a true
*/
function existeProducto($id){
    if(!is_numeric($id)){
        return false;
    }
    $consulta = "SELECT id from productos where id = $id";
    $articulo = read($consulta);
    return (bool)$articulo;
}//existeProducto()

/* recupera los datos de un dederminado producto
como el id es la clave primaria, solo se devolvera un resultado(o ninguno), 
por lo que retorna solamente el primer elemento del array (que es otro array o false)
*/
function detallesProducto($id){
    $consulta = "SELECT id,nombre,nombre_corto as nCorto,descripcion,pvp,familia FROM productos where id = $id";
    $detalles = read($consulta);
    return $detalles[0];
}//detallesProducto()

function borrarProducto($id){
    $consulta = "DELETE FROM productos WHERE id = $id";
    $borrado = accion($consulta);
    return (bool)$borrado;
}//borrarProducto()

function getFamilias(){
    $consulta = "SELECT cod, nombre FROM familias";
    $familias = read($consulta);
    return $familias;
}//getFamilias()

/*esta funcion utiliza un valor de retorno positivo
si la insercion se realizo con exito y un valor negativo
en caso contrario
para poder distinguir desde la llamada si ha retornado (con o sin error) y poder utilizar
el 0 si la funcion aun no ha sido llamada sin tener que crear otra variable expresamente
para tal fin.
*/
function crearProducto($nombre, $nCorto, $precio, $familia, $descripcion){
    $consulta = "INSERT INTO productos (nombre, nombre_corto, descripcion, pvp, familia) ".
    "VALUES ('$nombre', '$nCorto', '$descripcion', $precio, '$familia')";
    $insertado = accion($consulta);
    return $insertado?1:-1;
}//crearProducto()

function actualizarProducto($id, $nombre, $nCorto, $precio, $familia, $descripcion){
    $consulta = "UPDATE productos SET nombre ='$nombre',nombre_corto='$nCorto',".
    "descripcion='$descripcion' ,pvp=$precio, familia='$familia' WHERE id=$id";
    $actualizado = accion($consulta);
    return $actualizado;
}//actualizarProducto()