<?php
include_once 'boilerplate.php';
session_start();
$confirmacion = false;
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  $idioma = $_POST['idioma'];
  $publico = $_POST['publico'];
  $zona = $_POST['zona'];
  $confirmacion = true;
  $_SESSION['preferencias'] = ['idioma' => $idioma, 'publico' => $publico, 'zona' => $zona];
}
mostrarHTML();
if(isset($_SESSION['preferencias'])){
  mostrarFormulario($_SESSION['preferencias'],$confirmacion);
}else{
  mostrarFormulario();
}
mostrarEndTags();

function mostrarFormulario($preferencias = ['idioma' => 'en', 'publico '=> 'si', 'zona' => 'GMT'], $confirmacion = false){
  ?>
<div id="formulario" class="contenedor">
  <h2>Preferencias Usuario</h2>
  <hr>
  <p id="confirmacion">
    <?php 
    if($confirmacion)
      echo 'Preferencias de usuario guardadas.';
    ?>
  </p>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="idioma">Idioma</label><br>
    <div class="field">
      <img src="./icons/translate.svg" title="Idioma" alt="idioma">
      <select name="idioma">
        <option value="en" <?php echo $preferencias['idioma'] == 'en' ? 'selected="selected"':'' ?>>Inglés</option>
        <option value="es" <?php echo $preferencias['idioma'] == 'es' ? 'selected="selected"':'' ?>>Español</option>
      </select>
    </div><br>
    <label for="publico">Perfil Público</label><br>
    <div class="field">
      <img src="./icons/people-fill.svg" title="Perfil Publico" alt="people">
      <select name="publico">
        <option value="si" <?php echo $preferencias['publico'] == 'si' ? 'selected="selected"' : ''; ?>>Si</option>
        <option value="no" <?php echo $preferencias['publico'] == 'no' ? 'selected="selected"' : ''; ?>>No</option>
      </select>
    </div><br>
    <label for="zona">Zona horaria</label><br>
    <div class="field">
      <img src="./icons/clock.svg" title="Zona Horaria" alt="reloj">
      <select name="zona">
        <option value="GMT -2" <?php echo $preferencias['zona'] == 'GMT -2' ? 'selected="selected"' : ''; ?>>GMT-2</option>
        <option value="GMT -1" <?php echo $preferencias['zona'] == 'GMT -1' ? 'selected="selected"' : ''; ?>>GMT-1</option>
        <option value="GMT" <?php echo $preferencias['zona'] == 'GMT' ? 'selected="selected"' : ''; ?>>GMT</option>
        <option value="GMT +1" <?php echo $preferencias['zona'] == 'GMT +1' ? 'selected="selected"' : ''; ?>>GMT+1</option>
        <option value="GMT +2" <?php echo $preferencias['zona'] == 'GMT +2' ? 'selected="selected"' : ''; ?>>GMT+2</option>
      </select>
    </div><br>
    <div class="botones">
    <a class="boton neutro" href="mostrar.php">Mostrar Preferencias</a>
    <input type="submit" value="Establecer Preferencias" class="boton ok">
    </div>
  </form>
</div>
<?php
}//mostrarFormulario()
//lookus
?>