<?php
session_start();
if(!$_SESSION["agenda"]){   //si no hay datos de la agenda en la sesion, se define la variable y se cargan los datos de las cookies si los hubiera
    $_SESSION["agenda"]=array();    
    cargarCookies();
}
if($_SERVER["REQUEST_METHOD"] == "GET"){    // si se han enviado datos de algun formulario por get, se comprueba que se ha recibido y se actua en consecuencia
    if($_GET["vaciar"]){
        $_SESSION["agenda"]=array();
        //vaciarCookies();
    }
    if($_GET["guardar"]){
        guardarCookies();
    }
}
/*si se han recibido datos por post, se limpian los campos de texto y se comprueba que el nombre no esté vacio, y que haya algun dato en el telefono
 si hay nombre, se pone una cadena vacia en la variable que muestra el mensaje de error; si hay telefono se agrega (o modifica) el contacto, si no
 lo hay, se elimina el nombre (si existiese). Y por ultimo, si no hay nombre, se carga el mensaje de error en la variable que lo va a mostrar junto
 a su campo en el formulario.
*/
if($_SERVER["REQUEST_METHOD"] == "POST"){   
    $nombre = trim($_POST["nombre"]);
    $numero = trim($_POST["telefono"]);
    if($nombre){
        $nombreVacio = "";
        if($numero){
            agregarContacto($nombre,$numero);
        }else{
            eliminarContacto($nombre);
        }
    }else{
        $nombreVacio = "El Nombre es Obligatorio!!";
    }
}
?>
<!Doctype html>
<html>
    <meta charset="UTF-8"/>
    <head>
        <title>lucas Mónaco Fernández - DWES02 Tarea - Agenda</title>
<!-- un poquito de estilo para que se parezca a las imagenes del enunciado de la tarea -->        
        <style>
            body{
                background-color: #f77;
            }
            h1{
                text-align: center;
            }
            fieldset{
                width: 50%;
                margin: auto;
            }
            table{
                width:100%;
                padding:5px
            }
            input{
                width:40%;
                margin:5px;
            }
            label{
                width:max-content;
            }
            .error{
                color:red;
                font-weight: bold;
            }
            #agregar, #guardar{
                color:blue;
            }
            #vaciar{
                color:red;
            }
            #limpiar{
                color:green;
            }

        </style>
    </head>
    <body>
        <h1>Agenda</h1>
        <?php
        //si hay datos de la agenda en la sesion, se muestran los contactos usando la funcion a tal efecto
        if ($_SESSION["agenda"]){
            ?>
            <fieldset>
                <legend>Agenda:</legend>
               <?php
               echo agendaToHTML();
               ?>
            </fieldset>
        <?php
        }
        ?>
        <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="POST">
            <fieldset>
                <legend>Nuevo Contacto:</legend>
                <label for="nombre">Nombre: </label>
                <input type="text" name="nombre" id="nombre" required="true"/>
                <label for="nombre" class="error"><?php echo $nombreVacio;?></label><br/>
                <label for="telefono">Teléfono: </label>
                <input type="tel" name="telefono" id="telefono" pattern="[679]{1}[0-9]{8}"><br/>
                <input type="submit" value="Añadir Contacto" id="agregar"/>
                <input type="reset" value="Limpiar Campos" id="limpiar"/>
            </fieldset>
        </form>
        <?php
        //si hay datos en la variable agenda de la sesion, se muestra tambien el formulario para vaciar los datos
        //y el boton para guardar los cambios en las cookies. se usan campos ocultos para poder discernir que boton fue pulsado.
        if ($_SESSION["agenda"]){
            ?>
        <fieldset>
            <legend>Opciones de Agenda</legend>
            <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="GET">
                <input type="hidden" name="vaciar" value="true"/>
                <input type="submit" value="Vaciar" id="vaciar"/>
            </form>
            <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="GET">
                <input type="hidden" name="guardar" value="true"/>
                <input type="submit" value="Guardar Cambios" id="guardar"/>
            </form>
        </fieldset>
            <?php
        }
        ?>
        <!-- una pequeña explicacion para el profesor sobre las modificaciones realizadas al codigo a posteriori, 
            indicando tambien la extension de la funcionalidad usando cookies, pero que no interfieren con el funcionamiento
            de la pagina usando unicamente sesiones, con lo que se puede comprobar el funcionamiento de ambas maneras. -->
        <fieldset>
            <legend> Observaciones para el profesor </legend>
            <p>En primer lugar utilicé sesiones para guardar los datos, pero una vez cerrado el navegador,
                los datos de la sesion no perduraban (sí lo hacian si solo se cierra la pestaña pero no el navegador).</p>
            <p>Es por esto, que decidí usar cookies, pero en lugar de descartar el uso de las sesiones y reemplazarlas por las cookies,
                ya que estaba hecho y funcionando bien, agregué un boton al final que se encarga de volcar los datos de la sesion en las cookies.
            </p>
            <p>De esta forma se puede comprobar el funcionamiento de la pagina solo con las sesiones, y si se necesita persistencia,
                hacer uso de las cookies.
            </p>
            <p>Al cargar la pagina, se comprueba si ya existe el array con los datos de la agenda en la sesion, si no existe,
                se define uno vacio y se cargan los datos de las cookies en el array de la sesion. Se omite una cookie con 
                el nombre de "PHPSESSID" que por lo visto solo se crea cuando se usa una pestaña de incognito.
                <span style="text-decoration-line:line-through;"> El boton de vaciar, vacia tambien el contenido de las cookies con los datos de la agenda,
                y cuando se elimina un contacto, tambien se elimina de las cookies.
                Por lo demas, solo se trabaja con los datos de la sesion, tanto para mostrar los datos como para agregar uno nuevo o modificarlo.</span>
                Solo se trabaja con sesiones, y solo se vuelcan los datos en las cookies cuando se presiona el boton de "Guardar Cambios".
            </p>
        </fieldset>
        <?php echo firma();?>
    </body>
</html>
<?php
function agendaToHTML(){    //devuelve una tabla html con los datos de los contactos
    $contactos = "<table>";
    foreach($_SESSION["agenda"] as $nombre => $numero){
        $contactos.="<tr><td>".$nombre."</td><td>".$numero."</td></tr>";
    }
    $contactos.="</table>";
    return $contactos;
}
function agregarContacto($nombre, $numero){ //agrega un contacto nuevo a la agenda
    $_SESSION["agenda"][$nombre]=$numero;   //no hace falta comprobar si existe, porque si el nombre no existe, lo agrega, y si existe, lo sobreescribe
    ksort($_SESSION["agenda"]);
    
}
function eliminarContacto($nombre){     //elimina un contacto de la agenda si existe
    if(array_key_exists($nombre,$_SESSION["agenda"])){
        unset($_SESSION["agenda"][$nombre]);
    }
    //setcookie($nombre,"",time()-1);

}
function guardarCookies(){      //guarda los datos ACTUALES de la agenda en las cookies (vacia primero las existentes)
    vaciarCookies();
    foreach($_SESSION["agenda"] as $nombre => $numero){
        setcookie($nombre,$numero,time()+365*24*60*60);
    }
}
function cargarCookies(){   //carga los datos de las cookies en la agenda de la sesion actual
    foreach($_COOKIE as $nombre => $numero){
        if($nombre == "PHPSESSID") continue;
        $_SESSION["agenda"][$nombre] = $numero;
    }
}
function vaciarCookies(){   //elimina todas las cookies excepto la "PHPSESSID" 
    foreach($_COOKIE as $nombre){
        if($nombre == "PHPSESSID") continue;
        setcookie($nombre,"",time()-1);
    }
}
function firma(){   //devuelve un footer con estilo inline para la firma
    return "<footer style=\"position:fixed;right:5%;bottom:5%;opacity:0.3;\">lucas Mónaco Fernández</footer>";
}
?>